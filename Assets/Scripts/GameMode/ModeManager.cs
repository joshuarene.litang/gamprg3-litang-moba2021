﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpawnIt : UnityEvent<ModeManager> { };
public class ScaleCreeps : UnityEvent<ModeManager> { };
public class DebugDstryBrrcks : UnityEvent<DebugDstryBrrcks> { };

public enum WaveType
{
    Base,
    Siege,
};

public class ModeManager : MonoBehaviour
{
    private GameTimer timer;

    public SpawnIt SpawnSignal = new SpawnIt();
    public Creep son;
    public WaveType Wave;
    public GameObject DebugUI;
    
    //Manages Scores

    // Start is called before the first frame update
    void Start()
    {
        timer = GetComponent<GameTimer>();
        StartCoroutine("SpawnLoop");
    }

    // Update is called once per frame
    //place marker for hero to follow
    void Update()
    {
        DebugMenuShow();
    }

    public void DebugMenuShow()
    {
        if (Input.GetButtonDown("DebugMenu"))
        {
            if (DebugUI.activeSelf == false)
            {
                DebugUI.SetActive(true);
            }
            else
                DebugUI.SetActive(false);
        }
    }

    //Manages barracks spawning times
    void CreepWave()
    {
        if(timer.GetTime().y == 30f || timer.GetTime().y == 0f )
        {
            Wave = WaveType.Base;
            SpawnSignal.Invoke(this);
        }

        if (timer.GetTime().x % 5 == 0 && timer.GetTime().x != 0 && timer.GetTime().y == 0f)
        {
            Wave = WaveType.Siege;
            SpawnSignal.Invoke(this);
        }

        if (timer.GetTime().x % 7 == 0 && timer.GetTime().x != 0 && timer.GetTime().y == 30f)
        {
            Wave = WaveType.Base;
            Debug.Log("Scale the creep");
            SpawnSignal.Invoke(this);
        }
    }

    //This is a corouting because CreepWave() would be called during multiple frames
    private IEnumerator SpawnLoop()
    {
        Debug.Log("coroutinestarted");
        while (true)
        {
            CreepWave();
            yield return new WaitForSeconds(1f);
        }
        
        
    }

    //Manages kill bounties and exp distribution

    public void DebugDestruct()
    {
        Destroy(gameObject);
    }
}
