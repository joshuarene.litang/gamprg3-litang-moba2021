﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum BarrackType
{
    Melee,
    Ranged
}
public class Barracks : MonoBehaviour
{
    //For Referencing and caching
    private GameObject SendCreeps;
    private GameTimer Time;
    private List<CreepTraits> CachedTraits;

    public BarrackType Brrck;
    //reference of creeps to be spawned
    public List<Creep> Creeps;
    //used to cache a creep
    public Creep SpawnedUnit;
    //Holds onto the Transforms for the Creeps to follow after spawning
    public List<Transform> Route;
    //position where Creeps are spawned when instantiated
    public Transform SpawnPoint;
    
    void Start()
    {
        //Looks for the ModeManager then adds a listener
        if (SendCreeps == null)
        {
            SendCreeps = GameObject.FindGameObjectWithTag("Manager");
            SendCreeps.GetComponent<ModeManager>().SpawnSignal.AddListener(Spawn);
        }
        //Caches initial creeptraits
        for (int i = 0; i > Creeps.Count; i++)
        {
            CachedTraits.Add(Creeps[i].Traits);
        }
    }

    private void OnEnable()
    {
        
    }

    //For memory management
    private void OnDisable()
    {
        //SendCreeps = GameObject.FindGameObjectWithTag("Manager");
        SendCreeps.GetComponent<ModeManager>().SpawnSignal.RemoveListener(Spawn);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Spawns Creeps when called
    private void Spawn(ModeManager manager)
    {
        ////melee base
        //ScaleUnits(12, 1, 1, 0);
        ////ranged base
        //ScaleUnits(12, 2, 6, 8);
        ////melee super
        //ScaleUnits(19, 2, 2, 0);
        ////rangd super
        //ScaleUnits(18, 3, 6, 0);

        Time = manager.GetComponent<GameTimer>();

        if(manager.Wave == WaveType.Base) {


            if (Brrck == BarrackType.Melee) {
                StartCoroutine(BaseMeleeWave());
            }

             else if (Brrck == BarrackType.Ranged) {
                BaseRangedWave();
            }
        }

        if (manager.Wave == WaveType.Siege)
        {
            if (Brrck == BarrackType.Ranged) {
                StartCoroutine(BaseSiegeWave()); 
            }
        }
    }

    private IEnumerator BaseMeleeWave()
    {
        for (int i = 0; i < 3; i++)
        {
            SpawnedUnit = (Instantiate(Creeps[0], SpawnPoint.transform.position, Quaternion.identity));
            SpawnedUnit.Waypoints = Route;
            yield return new WaitForSeconds(0.5f);
        }
        
    }

    private void BaseRangedWave() {
        SpawnedUnit = (Instantiate(Creeps[0], SpawnPoint.transform.position, Quaternion.identity));
        SpawnedUnit.Waypoints = Route;
    }

    private IEnumerator BaseSiegeWave() {
        SpawnedUnit = (Instantiate(Creeps[0], SpawnPoint.transform.position, Quaternion.identity));
        SpawnedUnit.Waypoints = Route;
        yield return new WaitForSeconds(0.5f);
        SpawnedUnit = (Instantiate(Creeps[2], SpawnPoint.transform.position, Quaternion.identity));
        SpawnedUnit.Waypoints = Route;
    }

    //On button press all towers are destroyed
    private void DebugSelfDestruct()
    {

        if (Mathf.Sign(Input.GetAxis("Focus")) == 1)
        {
            Destroy(gameObject, 3f);
        }

    }

    //For scaling the creep scpecs and caching the previous values
    private CreepTraits ScaleUnits(CreepTraits trts, int hp, int ad, int gb, int eb)
    {
        trts.HealthPoints += hp;
        trts.AttackDamage += ad;
        trts.GoldBounty += gb;
        trts.ExpBounty += eb;
        return trts;
    }

}
