﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Tier {
    T1,
    T2,
    T3,
    T4
};

public class Towers : MonoBehaviour {
    //References
    public Tier TowerTier;
    public List<Towers> BackDoorProtector;
    public Collider[] Radar;
    public GameObject AttackProjectile;
    public GameObject Turret;
    public GameObject Target;
    public BuildingStats Stats;
    public LayerMask layermasks;

    private Health healthPoints;
    private GameObject shot;

    // Start is called before the first frame update
    void Start() {
        layermasks = 1 << 8;
        StartCoroutine("Fire");
        healthPoints = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update() {
        if (healthPoints.GetDead())
        {
            Destroy(gameObject, 0.5f);
        }
    }

    private void FixedUpdate() {
        Targeting();

    }

    //Detect and assign target
    private void Targeting() {
        if (Target != null) {
            if (Vector3.Distance(transform.position, Target.transform.position) > Stats.AttackRange)
            {
                Target = null;
            }
        }
        Collider[] Radar = Physics.OverlapSphere(transform.position, Stats.AttackRange, layermasks);

        foreach (Collider opponent in Radar) {
            if (opponent.gameObject.tag == "Creep")
            {
                Debug.Log(opponent.name);
                Target = opponent.gameObject;
                
            }

            //if (opponent.gameObject.tag == "Creep" && Target == null)
            //{
            //    Target = opponent.gameObject;
            //    Debug.Log(opponent.gameObject.name);
            //}
        }
    }

    //After getting a target the tower will fire
    private IEnumerator Fire() {
        if (Target != null)  {
            Debug.Log("Shot");
            TowerProjectile tracker;

            shot = Instantiate(AttackProjectile, Turret.transform.position, Quaternion.identity);

            tracker = shot.gameObject.GetComponent<TowerProjectile>();
            tracker.Target = Target.transform;
            //if (Vector3.Distance(transform.position, Target.transform.position) > Stats.AttackRange)
            //{
            //    Target = null;
            //    Debug.Log("Target Decouple works");
            //}
        }

        yield return new WaitForSeconds(Stats.BasicAttackTime);
    }
    
    void SortList(List<Transform> enemies)
    {

    }

    //For destroying all Towers on button press
    private void DebugSelfDestroy() {

    }

    //To visualize the Tower's attack/sight range
    private void OnDrawGizmosSelected() {
        Gizmos.DrawWireSphere(transform.position, Stats.AttackRange);
    }
}
