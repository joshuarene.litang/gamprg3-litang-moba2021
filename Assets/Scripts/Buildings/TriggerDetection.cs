﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerDetection : MonoBehaviour
{
    [SerializeField]
    public UnityEvent<GameObject> Detected;
    //public Detected foreigner = new Detected();
    // Start is called before the first frame update

    private void OnCollisionEnter(Collision collision)
    {
        if(Detected != null)
        {
            Detected.Invoke(collision.gameObject);
        }
    }
        

    private void OnCollisionExit(Collision collision)
    {
        
    }
}
