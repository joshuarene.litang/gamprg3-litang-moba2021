﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerProjectile : Projectile
{
    public Transform Target;
    public float RotationSpeed = 3f;
    public float FlightSpeed = 10f;

    private Quaternion rotateToTarget;
    private Vector3 dir;
    private Rigidbody rb;


    void Start() {
        OnLaunched();
    }
    public override void OnLaunched()
    {
        rb = GetComponent<Rigidbody>();
        dir = (Target.transform.position - transform.position).normalized;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        rotateToTarget = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotateToTarget, Time.deltaTime * RotationSpeed);
    }

    public override void InAir()
    {
        
    }

    public override void OnContact()
    {
        
        //GameObject player = collision.gameObject;

        //if (player != null) {
        //    gameObject.SetActive(false);
        //}
        //else {
        //    //Destroy(gameObject, 3f);
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        
    }

    // Use this for initialization
    

    // Update is called once per frame
    void Update() {
        PursueTarget();
    }

    void PursueTarget() {
        dir = (Target.transform.position - transform.position).normalized;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        rotateToTarget = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotateToTarget, Time.deltaTime * RotationSpeed);
        rb.velocity = new Vector2(dir.x * FlightSpeed, dir.y * FlightSpeed);
    }
}
