﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
[ExecuteAlways]
public class DayNightCycle : MonoBehaviour
{
    // light References
    [SerializeField]
    private Light DirectionalLight;
    [SerializeField]
    private LightingPreset Preset;
    public GameTimer TrueTime;
    [SerializeField, Range(0, 600)]
    //variables
    public float TimeOfDay;

    private void Start()
    {
        DirectionalLight.transform.Rotate(0, 0, 0);
    }

    private void Update()
    {
        if (Preset == null)
            return;

        if (Application.isPlaying)
        {
            //(Replace with a reference to the game time)
            TimeOfDay += Time.deltaTime;
            TimeOfDay %= 600f; //Modulus to ensure always between 0-600
            UpdateLighting(TimeOfDay / 600f);


        }
        else
        {
            UpdateLighting(TimeOfDay / 600f);
        }
    }


    private void UpdateLighting(float timePercent)
    {
        //Set ambient and fog
        RenderSettings.ambientLight = Preset.AmbientColor.Evaluate(timePercent);
        RenderSettings.fogColor = Preset.FogColor.Evaluate(timePercent);

        //If the directional light is set then rotate and set it's color, I actually rarely use the rotation because it casts tall shadows unless you clamp the value
        if (DirectionalLight != null)
        {
            DirectionalLight.color = Preset.DirectionalColor.Evaluate(timePercent);

            DirectionalLight.transform.localRotation = Quaternion.Euler(new Vector3((timePercent * 360f) - 90f, 170f, 0));
        }
    }
}

//source: https://gist.github.com/Glynn-Taylor/08da28896147faa6ba8f9654057d38e6
