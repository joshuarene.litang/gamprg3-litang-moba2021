﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimeUI : MonoBehaviour
{
    public Text UiText;
    public GameTimer Watch;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ClockText();
    }

    private void ClockText()
    {
        string minute = Watch.GetTime().x.ToString();
        string seconds = Watch.GetTime().y.ToString();

        if (Watch.GetTime().x < 10)
        {
            minute = minute.Insert(0, "0");
        }
        if (Watch.GetTime().y < 10)
        {
            seconds = seconds.Insert(0, "0");
        }

        UiText.text = minute + ':' + seconds;
    }
}
