﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovementState : StateMachineBehaviour
{
//    state machine
//one by one
//redo blackboard
//add behaviors

//detection
//make script
//make event
//ontrigger
//use editor start function
    //public GameObject MarkerPrefab;

    [SerializeField]
    //private GameObject heroMarker;
    public AudioSource audioData;
    public NavMeshAgent agent;
    public Animator anim;
    private Hero Actor;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //cache anim and agent
        //set destination
        Actor = animator.GetComponent<Hero>();
        agent = animator.GetComponent<NavMeshAgent>();
        anim = animator.GetComponent<Animator>();
        audioData = Actor.audioData.GetComponent<AudioSource>();
        AudioClip myClip = AudioClip.Create("Moving", 44100 * 2, 1, 44100, true);
        audioData.clip = myClip;
        audioData.Play(0);
    }
        

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        anim.SetBool("Moving", false);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (agent.isStopped)
        {
            //agent.ResetPath();
            
        }
        //change moving state bool
        if (Actor.Waypoint != Actor.transform.position)
        {
            agent.SetDestination(Actor.Waypoint);
        }
    }

    
}
