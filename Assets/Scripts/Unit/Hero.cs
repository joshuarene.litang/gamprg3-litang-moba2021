﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Hero : MonoBehaviour {
    //public Attribute traits;
    [SerializeField]
    public Transform AttackPoint;
    public GameObject MarkerPrefab;
    public Vector3 Waypoint;

    [SerializeField]
    private GameObject heroMarker;

    protected NavMeshAgent agent;
    [SerializeField]
    protected LayerMask sightLayerMasks;
    protected Animator anim;
    public GameObject Target;
    public AudioSource audioData;
    //states
    protected MovementState movingState;
    
    // Start is called before the first frame update
    protected void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        movingState = anim.GetBehaviour<MovementState>();
        Target = null;
        audioData = GetComponent<AudioSource>();
        audioData.Play(0);
    }

    // Update is called once per frame
    protected void Update()
    {
        MarkerPlacement();
        BasicAttack();
        Stop();
    }

    protected void MarkerPlacement()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetButton("Fire2"))
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~LayerMask.GetMask("Default", "Combatant")))
            {
                
                // Use a RaycastHit to see where you hit via hit.point (where hit is a variable name)
                // use hit.point to move Hero
                //print("Hit something!");
                Debug.DrawLine(ray.origin, hit.point);
                // If marker is not null, reposition
                if (heroMarker != null)
                {
                    heroMarker.SetActive(true);
                    heroMarker.transform.position = hit.point;
                }

                // Else if marker is null, instantiate
                else if (heroMarker == null)
                {
                    heroMarker = Instantiate(MarkerPrefab, Input.mousePosition, transform.rotation);
                    heroMarker.transform.position = hit.point;
                }

                // Send Hero marker location
                anim.SetBool("Moving", true);
                anim.SetBool("HasTarget", false);
                Target = null;
                Waypoint = hit.point;
                agent.isStopped = false;
                //agent.SetDestination(hit.point);
            }
        }
        //if hero is near/on the marker disable marker for next use
        if (Vector3.Distance(heroMarker.transform.position, transform.position) < 1.0f)
        {
            heroMarker.SetActive(false);
            //Waypoint = transform.position;
            anim.SetBool("Moving", false);
        }
    }

    protected virtual void Movement()
    {
        
    }

    protected virtual void Die()
    {
        GetComponent<GameObject>().SetActive(false);
    }

    protected void BasicAttack()
    {
        if (Input.GetButton("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~LayerMask.GetMask("Default", "Terrain")))
            {
                // Use a RaycastHit to see where you hit via hit.point (where hit is a variable name)
                // use hit.point to move Hero
                //print("Hit something!");
                Debug.DrawLine(ray.origin, hit.point);
                // If marker is not null, reposition
                if (hit.collider.gameObject.tag == "Creep")
                {
                    Debug.Log(hit.collider.gameObject.name);
                    anim.SetBool("HasTarget", true);
                    Target = hit.collider.gameObject;
                }

            }
            
        }
        if (Target != null)
        {
            if (Vector3.Distance(transform.position, Target.transform.position) > 4.0f)
            {
                //agent.SetDestination(Target.transform.forward);
                anim.SetBool("Moving", true);
                anim.SetBool("InRange", false);
            }
        }
    }

    void Stop()
    {
        if (Input.GetButton("Stop"))
        {
            anim.SetBool("Moving", false);
            heroMarker.SetActive(false);
            Target = null;
            anim.SetBool("HasTarget", false);
            
        }
    }
    protected virtual void Idle()
    {

    }
}
