﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float MaxHealth;
    private float currHealth;

    private bool IsDead;

    // Use this for initialization
    void Start()
    {
        currHealth = MaxHealth;
        currHealth = Mathf.Clamp(currHealth, 0, MaxHealth);
        IsDead = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(float damage)
    {
        currHealth -= damage;
        currHealth = Mathf.Clamp(currHealth, 0, MaxHealth);
        if (currHealth <= 0f)
        {
            IsDead = true;
        }
    }

    public void Heal(float healing)
    {
        currHealth += healing;
        currHealth = Mathf.Clamp(currHealth, 0, MaxHealth);
    }

    public void NewMaxHealth(float newHealth)
    {
        MaxHealth = newHealth;
        currHealth = Mathf.Clamp(currHealth, 0, MaxHealth);
        Start();
    }

    public bool GetDead()
    {
        //return
        return IsDead;
    }

    private IEnumerator OnDeath()
    {
        // Turn the tank off.
        //Dead = true;
        yield return new WaitForSeconds(1.0f);
        gameObject.SetActive(false);
    }
}
